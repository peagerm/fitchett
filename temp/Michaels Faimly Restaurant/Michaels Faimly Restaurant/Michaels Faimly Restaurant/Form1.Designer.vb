﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.ListBox1 = New System.Windows.Forms.ListBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.btngettotal = New System.Windows.Forms.Button()
        Me.btnneworder = New System.Windows.Forms.Button()
        Me.btnexit = New System.Windows.Forms.Button()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.btncomo2 = New System.Windows.Forms.Button()
        Me.btncombo3 = New System.Windows.Forms.Button()
        Me.btncombo4 = New System.Windows.Forms.Button()
        Me.ListBox2 = New System.Windows.Forms.ListBox()
        Me.Button8 = New System.Windows.Forms.Button()
        Me.ListBox = New System.Windows.Forms.ListBox()
        Me.btncombo1 = New System.Windows.Forms.Button()
        Me.btnhamburger = New System.Windows.Forms.Button()
        Me.btncheeseburger = New System.Windows.Forms.Button()
        Me.btndoublecheeseburger = New System.Windows.Forms.Button()
        Me.btnhalfpound = New System.Windows.Forms.Button()
        Me.btntaco = New System.Windows.Forms.Button()
        Me.btnchef = New System.Windows.Forms.Button()
        Me.btnhouse = New System.Windows.Forms.Button()
        Me.btnsmallfries = New System.Windows.Forms.Button()
        Me.btnlargefries = New System.Windows.Forms.Button()
        Me.btnonion = New System.Windows.Forms.Button()
        Me.btnsmalldrink = New System.Windows.Forms.Button()
        Me.btnmediumdrink = New System.Windows.Forms.Button()
        Me.btnlargedrink = New System.Windows.Forms.Button()
        Me.btnsweet = New System.Windows.Forms.Button()
        Me.btnunsweet = New System.Windows.Forms.Button()
        Me.btnwater = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(379, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(488, 36)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Michael's Family Restaurant"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'ListBox1
        '
        Me.ListBox1.FormattingEnabled = True
        Me.ListBox1.Location = New System.Drawing.Point(26, 83)
        Me.ListBox1.Name = "ListBox1"
        Me.ListBox1.Size = New System.Drawing.Size(220, 446)
        Me.ListBox1.TabIndex = 1
        '
        'Label2
        '
        Me.Label2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(345, 58)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(78, 40)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "Combos"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label3
        '
        Me.Label3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(470, 58)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(78, 40)
        Me.Label3.TabIndex = 0
        Me.Label3.Text = "Burgers"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label4
        '
        Me.Label4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(592, 58)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(78, 40)
        Me.Label4.TabIndex = 0
        Me.Label4.Text = "Salads"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label5
        '
        Me.Label5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(709, 58)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(78, 40)
        Me.Label5.TabIndex = 0
        Me.Label5.Text = "Sides"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label6
        '
        Me.Label6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(822, 58)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(78, 40)
        Me.Label6.TabIndex = 0
        Me.Label6.Text = "Drinks"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btngettotal
        '
        Me.btngettotal.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btngettotal.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.btngettotal.Location = New System.Drawing.Point(379, 415)
        Me.btngettotal.MaximumSize = New System.Drawing.Size(112, 0)
        Me.btngettotal.Name = "btngettotal"
        Me.btngettotal.Size = New System.Drawing.Size(112, 48)
        Me.btngettotal.TabIndex = 2
        Me.btngettotal.Text = "&Get Total"
        Me.btngettotal.UseVisualStyleBackColor = True
        '
        'btnneworder
        '
        Me.btnneworder.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnneworder.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.btnneworder.Location = New System.Drawing.Point(523, 415)
        Me.btnneworder.MaximumSize = New System.Drawing.Size(112, 0)
        Me.btnneworder.Name = "btnneworder"
        Me.btnneworder.Size = New System.Drawing.Size(112, 48)
        Me.btnneworder.TabIndex = 2
        Me.btnneworder.Text = "&New Order"
        Me.btnneworder.UseVisualStyleBackColor = True
        '
        'btnexit
        '
        Me.btnexit.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnexit.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.btnexit.Location = New System.Drawing.Point(662, 415)
        Me.btnexit.MaximumSize = New System.Drawing.Size(112, 0)
        Me.btnexit.Name = "btnexit"
        Me.btnexit.Size = New System.Drawing.Size(112, 48)
        Me.btnexit.TabIndex = 2
        Me.btnexit.Text = "&Exit"
        Me.btnexit.UseVisualStyleBackColor = True
        '
        'Button4
        '
        Me.Button4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button4.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Button4.Location = New System.Drawing.Point(335, 120)
        Me.Button4.MaximumSize = New System.Drawing.Size(112, 0)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(99, 48)
        Me.Button4.TabIndex = 2
        Me.Button4.Text = "Button1"
        Me.Button4.UseVisualStyleBackColor = True
        '
        'btncomo2
        '
        Me.btncomo2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btncomo2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.btncomo2.Location = New System.Drawing.Point(335, 179)
        Me.btncomo2.MaximumSize = New System.Drawing.Size(112, 0)
        Me.btncomo2.Name = "btncomo2"
        Me.btncomo2.Size = New System.Drawing.Size(99, 48)
        Me.btncomo2.TabIndex = 2
        Me.btncomo2.Text = "Combo #2"
        Me.btncomo2.UseVisualStyleBackColor = True
        '
        'btncombo3
        '
        Me.btncombo3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btncombo3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.btncombo3.Location = New System.Drawing.Point(335, 238)
        Me.btncombo3.MaximumSize = New System.Drawing.Size(112, 0)
        Me.btncombo3.Name = "btncombo3"
        Me.btncombo3.Size = New System.Drawing.Size(99, 48)
        Me.btncombo3.TabIndex = 2
        Me.btncombo3.Text = "Combo #3"
        Me.btncombo3.UseVisualStyleBackColor = True
        '
        'btncombo4
        '
        Me.btncombo4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btncombo4.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.btncombo4.Location = New System.Drawing.Point(335, 297)
        Me.btncombo4.MaximumSize = New System.Drawing.Size(112, 0)
        Me.btncombo4.Name = "btncombo4"
        Me.btncombo4.Size = New System.Drawing.Size(99, 48)
        Me.btncombo4.TabIndex = 2
        Me.btncombo4.Text = "Combo #4"
        Me.btncombo4.UseVisualStyleBackColor = True
        '
        'ListBox2
        '
        Me.ListBox2.FormattingEnabled = True
        Me.ListBox2.Location = New System.Drawing.Point(26, 83)
        Me.ListBox2.Name = "ListBox2"
        Me.ListBox2.Size = New System.Drawing.Size(220, 446)
        Me.ListBox2.TabIndex = 1
        '
        'Button8
        '
        Me.Button8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button8.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Button8.Location = New System.Drawing.Point(335, 120)
        Me.Button8.MaximumSize = New System.Drawing.Size(112, 0)
        Me.Button8.Name = "Button8"
        Me.Button8.Size = New System.Drawing.Size(99, 48)
        Me.Button8.TabIndex = 2
        Me.Button8.Text = "Button1"
        Me.Button8.UseVisualStyleBackColor = True
        '
        'ListBox
        '
        Me.ListBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.ListBox.FormattingEnabled = True
        Me.ListBox.Location = New System.Drawing.Point(26, 83)
        Me.ListBox.Name = "ListBox"
        Me.ListBox.Size = New System.Drawing.Size(220, 446)
        Me.ListBox.TabIndex = 1
        '
        'btncombo1
        '
        Me.btncombo1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btncombo1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.btncombo1.Location = New System.Drawing.Point(335, 120)
        Me.btncombo1.MaximumSize = New System.Drawing.Size(112, 0)
        Me.btncombo1.Name = "btncombo1"
        Me.btncombo1.Size = New System.Drawing.Size(99, 48)
        Me.btncombo1.TabIndex = 2
        Me.btncombo1.Text = "Combo #1"
        Me.btncombo1.UseVisualStyleBackColor = True
        '
        'btnhamburger
        '
        Me.btnhamburger.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnhamburger.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.btnhamburger.Location = New System.Drawing.Point(461, 120)
        Me.btnhamburger.MaximumSize = New System.Drawing.Size(112, 0)
        Me.btnhamburger.Name = "btnhamburger"
        Me.btnhamburger.Size = New System.Drawing.Size(99, 48)
        Me.btnhamburger.TabIndex = 2
        Me.btnhamburger.Text = "Hamburger"
        Me.btnhamburger.UseVisualStyleBackColor = True
        '
        'btncheeseburger
        '
        Me.btncheeseburger.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btncheeseburger.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.btncheeseburger.Location = New System.Drawing.Point(461, 179)
        Me.btncheeseburger.MaximumSize = New System.Drawing.Size(112, 0)
        Me.btncheeseburger.Name = "btncheeseburger"
        Me.btncheeseburger.Size = New System.Drawing.Size(99, 48)
        Me.btncheeseburger.TabIndex = 2
        Me.btncheeseburger.Text = "Cheeseburger"
        Me.btncheeseburger.UseVisualStyleBackColor = True
        '
        'btndoublecheeseburger
        '
        Me.btndoublecheeseburger.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btndoublecheeseburger.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.btndoublecheeseburger.Location = New System.Drawing.Point(461, 238)
        Me.btndoublecheeseburger.MaximumSize = New System.Drawing.Size(112, 0)
        Me.btndoublecheeseburger.Name = "btndoublecheeseburger"
        Me.btndoublecheeseburger.Size = New System.Drawing.Size(99, 48)
        Me.btndoublecheeseburger.TabIndex = 2
        Me.btndoublecheeseburger.Text = "Double Cheeseburger"
        Me.btndoublecheeseburger.UseVisualStyleBackColor = True
        '
        'btnhalfpound
        '
        Me.btnhalfpound.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnhalfpound.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.btnhalfpound.Location = New System.Drawing.Point(461, 297)
        Me.btnhalfpound.MaximumSize = New System.Drawing.Size(112, 0)
        Me.btnhalfpound.Name = "btnhalfpound"
        Me.btnhalfpound.Size = New System.Drawing.Size(99, 48)
        Me.btnhalfpound.TabIndex = 2
        Me.btnhalfpound.Text = "Half Pound Cheeseburger"
        Me.btnhalfpound.UseVisualStyleBackColor = True
        '
        'btntaco
        '
        Me.btntaco.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btntaco.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.btntaco.Location = New System.Drawing.Point(584, 120)
        Me.btntaco.MaximumSize = New System.Drawing.Size(112, 0)
        Me.btntaco.Name = "btntaco"
        Me.btntaco.Size = New System.Drawing.Size(99, 48)
        Me.btntaco.TabIndex = 2
        Me.btntaco.Text = "Taco Salad"
        Me.btntaco.UseVisualStyleBackColor = True
        '
        'btnchef
        '
        Me.btnchef.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnchef.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.btnchef.Location = New System.Drawing.Point(584, 179)
        Me.btnchef.MaximumSize = New System.Drawing.Size(112, 0)
        Me.btnchef.Name = "btnchef"
        Me.btnchef.Size = New System.Drawing.Size(99, 48)
        Me.btnchef.TabIndex = 2
        Me.btnchef.Text = "Chef Salad"
        Me.btnchef.UseVisualStyleBackColor = True
        '
        'btnhouse
        '
        Me.btnhouse.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnhouse.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.btnhouse.Location = New System.Drawing.Point(584, 238)
        Me.btnhouse.MaximumSize = New System.Drawing.Size(112, 0)
        Me.btnhouse.Name = "btnhouse"
        Me.btnhouse.Size = New System.Drawing.Size(99, 48)
        Me.btnhouse.TabIndex = 2
        Me.btnhouse.Text = "House Salad"
        Me.btnhouse.UseVisualStyleBackColor = True
        '
        'btnsmallfries
        '
        Me.btnsmallfries.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnsmallfries.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.btnsmallfries.Location = New System.Drawing.Point(702, 120)
        Me.btnsmallfries.MaximumSize = New System.Drawing.Size(112, 0)
        Me.btnsmallfries.Name = "btnsmallfries"
        Me.btnsmallfries.Size = New System.Drawing.Size(99, 48)
        Me.btnsmallfries.TabIndex = 2
        Me.btnsmallfries.Text = "Small Fries"
        Me.btnsmallfries.UseVisualStyleBackColor = True
        '
        'btnlargefries
        '
        Me.btnlargefries.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnlargefries.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.btnlargefries.Location = New System.Drawing.Point(702, 179)
        Me.btnlargefries.MaximumSize = New System.Drawing.Size(112, 0)
        Me.btnlargefries.Name = "btnlargefries"
        Me.btnlargefries.Size = New System.Drawing.Size(99, 48)
        Me.btnlargefries.TabIndex = 2
        Me.btnlargefries.Text = "Large Fries"
        Me.btnlargefries.UseVisualStyleBackColor = True
        '
        'btnonion
        '
        Me.btnonion.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnonion.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.btnonion.Location = New System.Drawing.Point(702, 238)
        Me.btnonion.MaximumSize = New System.Drawing.Size(112, 0)
        Me.btnonion.Name = "btnonion"
        Me.btnonion.Size = New System.Drawing.Size(99, 48)
        Me.btnonion.TabIndex = 2
        Me.btnonion.Text = "Onion Rings"
        Me.btnonion.UseVisualStyleBackColor = True
        '
        'btnsmalldrink
        '
        Me.btnsmalldrink.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnsmalldrink.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.btnsmalldrink.Location = New System.Drawing.Point(819, 120)
        Me.btnsmalldrink.MaximumSize = New System.Drawing.Size(112, 0)
        Me.btnsmalldrink.Name = "btnsmalldrink"
        Me.btnsmalldrink.Size = New System.Drawing.Size(99, 48)
        Me.btnsmalldrink.TabIndex = 2
        Me.btnsmalldrink.Text = "Small Soft Drink"
        Me.btnsmalldrink.UseVisualStyleBackColor = True
        '
        'btnmediumdrink
        '
        Me.btnmediumdrink.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnmediumdrink.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.btnmediumdrink.Location = New System.Drawing.Point(819, 179)
        Me.btnmediumdrink.MaximumSize = New System.Drawing.Size(112, 0)
        Me.btnmediumdrink.Name = "btnmediumdrink"
        Me.btnmediumdrink.Size = New System.Drawing.Size(99, 48)
        Me.btnmediumdrink.TabIndex = 2
        Me.btnmediumdrink.Text = "Medium Soft Drink"
        Me.btnmediumdrink.UseVisualStyleBackColor = True
        '
        'btnlargedrink
        '
        Me.btnlargedrink.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnlargedrink.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.btnlargedrink.Location = New System.Drawing.Point(819, 238)
        Me.btnlargedrink.MaximumSize = New System.Drawing.Size(112, 0)
        Me.btnlargedrink.Name = "btnlargedrink"
        Me.btnlargedrink.Size = New System.Drawing.Size(99, 48)
        Me.btnlargedrink.TabIndex = 2
        Me.btnlargedrink.Text = "Large Soft Drink"
        Me.btnlargedrink.UseVisualStyleBackColor = True
        '
        'btnsweet
        '
        Me.btnsweet.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnsweet.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.btnsweet.Location = New System.Drawing.Point(819, 297)
        Me.btnsweet.MaximumSize = New System.Drawing.Size(112, 0)
        Me.btnsweet.Name = "btnsweet"
        Me.btnsweet.Size = New System.Drawing.Size(99, 48)
        Me.btnsweet.TabIndex = 2
        Me.btnsweet.Text = "Sweet Tea"
        Me.btnsweet.UseVisualStyleBackColor = True
        '
        'btnunsweet
        '
        Me.btnunsweet.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnunsweet.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.btnunsweet.Location = New System.Drawing.Point(819, 356)
        Me.btnunsweet.MaximumSize = New System.Drawing.Size(112, 0)
        Me.btnunsweet.Name = "btnunsweet"
        Me.btnunsweet.Size = New System.Drawing.Size(99, 48)
        Me.btnunsweet.TabIndex = 2
        Me.btnunsweet.Text = "Unsweet Tea"
        Me.btnunsweet.UseVisualStyleBackColor = True
        '
        'btnwater
        '
        Me.btnwater.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnwater.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.btnwater.Location = New System.Drawing.Point(819, 415)
        Me.btnwater.MaximumSize = New System.Drawing.Size(112, 0)
        Me.btnwater.Name = "btnwater"
        Me.btnwater.Size = New System.Drawing.Size(99, 48)
        Me.btnwater.TabIndex = 2
        Me.btnwater.Text = "Water"
        Me.btnwater.UseVisualStyleBackColor = True
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(1130, 568)
        Me.Controls.Add(Me.btnexit)
        Me.Controls.Add(Me.btnneworder)
        Me.Controls.Add(Me.btncombo4)
        Me.Controls.Add(Me.btncombo3)
        Me.Controls.Add(Me.btncomo2)
        Me.Controls.Add(Me.btnhalfpound)
        Me.Controls.Add(Me.btndoublecheeseburger)
        Me.Controls.Add(Me.btncheeseburger)
        Me.Controls.Add(Me.btnhouse)
        Me.Controls.Add(Me.btnchef)
        Me.Controls.Add(Me.btnonion)
        Me.Controls.Add(Me.btnlargefries)
        Me.Controls.Add(Me.btnwater)
        Me.Controls.Add(Me.btnunsweet)
        Me.Controls.Add(Me.btnsweet)
        Me.Controls.Add(Me.btnlargedrink)
        Me.Controls.Add(Me.btnmediumdrink)
        Me.Controls.Add(Me.btnsmalldrink)
        Me.Controls.Add(Me.btnsmallfries)
        Me.Controls.Add(Me.btntaco)
        Me.Controls.Add(Me.btnhamburger)
        Me.Controls.Add(Me.btncombo1)
        Me.Controls.Add(Me.Button8)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.ListBox)
        Me.Controls.Add(Me.btngettotal)
        Me.Controls.Add(Me.ListBox2)
        Me.Controls.Add(Me.ListBox1)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Name = "Form1"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Michaels Menu"
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents Label1 As Label
    Friend WithEvents ListBox1 As ListBox
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents btngettotal As Button
    Friend WithEvents btnneworder As Button
    Friend WithEvents btnexit As Button
    Friend WithEvents Button4 As Button
    Friend WithEvents btncomo2 As Button
    Friend WithEvents btncombo3 As Button
    Friend WithEvents btncombo4 As Button
    Friend WithEvents ListBox2 As ListBox
    Friend WithEvents Button8 As Button
    Friend WithEvents ListBox As ListBox
    Friend WithEvents btncombo1 As Button
    Friend WithEvents btnhamburger As Button
    Friend WithEvents btncheeseburger As Button
    Friend WithEvents btndoublecheeseburger As Button
    Friend WithEvents btnhalfpound As Button
    Friend WithEvents btntaco As Button
    Friend WithEvents btnchef As Button
    Friend WithEvents btnhouse As Button
    Friend WithEvents btnsmallfries As Button
    Friend WithEvents btnlargefries As Button
    Friend WithEvents btnonion As Button
    Friend WithEvents btnsmalldrink As Button
    Friend WithEvents btnmediumdrink As Button
    Friend WithEvents btnlargedrink As Button
    Friend WithEvents btnsweet As Button
    Friend WithEvents btnunsweet As Button
    Friend WithEvents btnwater As Button
End Class
